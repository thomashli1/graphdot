#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
from scipy.sparse.linalg import bicgstab
from scipy.optimize import minimize


class GaussianFieldRegressor:
    '''Semi-supervised learning and prediction of missing labels of continuous
    value on a graph. Reference: Zhu, Ghahramani, Lafferty. ICML 2003

    Parameters
    ----------
    weight: functor or 'precomputed'
        A function that implements a weight function that converts distance
        to graph edge weights. The value of a weight function should generally
        decay with distance. If weight is 'precomputed', then the result as
        computed by `metric` will be directly used as weight.
    optimizer: one of (sstr, True, None, callable)
        A string or callable that represents one of the optimizers usable in
        the scipy.optimize.minimize method.
        if None, no hyperparameter optimization will be carried out in fitting.
        If True, the optimizer will default to L-BFGS-B.
    smoothing: float in [0, 1)
        Float that controls the smoothing of the transition matrix. Used for
        regularization.
    eps: float in [0, 1)
        Controls the step size for approximating gradients with finite
        difference.
    eta: float in [0, 1)
        Controls a dongle implementation. When eta > 0, each node will have
        a copy made attached with an edge weight of eta * the total weight
        of all other nodes attached to the original. The model will then
        treat the original node as an unlabeled node. Used for regularization.
    '''

    def __init__(self, weight, optimizer=None, smoothing=1e-3, eps=1e-3, eta=0):
        self.weight = weight
        self.optimizer = optimizer
        if optimizer is True:
            self.optimizer = 'L-BFGS-B'
        self.data = None
        self.labels = None
        self.eps = eps
        if smoothing < 0 or smoothing > 1:
            raise Exception("Smoothing must be between 0 and 1 inclusive.")
        self.smoothing = smoothing
        if eta < 0 or eta > 1:
            raise Exception("Eta must be between 0 and 1 inclusive.")
        self.eta = eta

    def set_model(self, X, y):
        '''Input training data into the model

        Parameters
        ----------
        X: 2D array or list of objects
            Feature vectors or other generic representaations of labeled data.
        y: 1D array
            Labels/target values.
        '''
        self.X = X
        self.f_l = y

    def fit(self, Z=None, y=None, loss=None, options=None):
        '''Train the Gaussian field model.

        Parameters
        ----------
        X: 2D array or list of objects
            Feature vectors or other generic representations of labeled data.
        y: 1D array
            Labels/target values.
        append_data: boolean
            Whether or not to add fitted data to the model
        '''
        if len(self.X) == 0:
            raise Exception("Missing Training Data")
        if len(self.f_l) == 0:
            raise Exception("Missing Training Labels")
        if self.weight.theta is not None and self.optimizer:
            if loss == 'mse':
                func = lambda theta, self=self: self.squared_error(
                    Z, y, theta, eval_gradient=True
                )
            elif loss == 'cross_entropy':
                func = lambda theta, self=self: self.cross_entropy(
                    Z, y, theta, eval_gradient=True
                )
            elif loss == 'laplacian':
                func = lambda theta, self=self: self.laplacian_error(
                    theta, eval_gradient=True
                )
            elif loss == 'smooth':
                func = lambda theta, self=self: self.average_label_entropy_(
                    Z, y, theta, eval_gradient=True
                )
            else:
                func = lambda theta, self=self: self.average_label_entropy(
                    Z, y, theta, eval_gradient=True
                )
            opt = minimize(
                    fun=func,
                    method=self.optimizer,
                    x0=self.weight.theta,
                    bounds=self.weight.bounds,
                    jac=True,
                    tol=1e-5,
                    options=options
                )
            if opt.success:
                self.weight.theta = opt.x
            else:
                raise RuntimeError(
                    f'Optimizer did not converge, got:\n'
                    f'{opt}'
                )

    def predict(self, Z, display=False):
        '''Predict using the trained Gaussian field model.

        Parameters
        ----------
        Z: 2D array or list of objects
            Feature vectors or other generic representations of unlabeled data.

        display: boolean
            Whether or not to display the label weightings and predictive
            uncertainty.

        Returns
        -------
        y: 1D array
            Predicted values of the input data.
        influence_matrix: 2D array
            Contributions from each labeled sample for each predicted label.
        predictive_uncertainty: 1D array
            Weighted Standard Deviation of the predicted labels.
        '''
        if len(self.X) == 0:
            raise Exception("Missing Training Data")
        if len(self.f_l) == 0:
            raise Exception("Missing Training Labels")
        if len(Z) == 0:
            return []

        X = self.X
        f_l = self.f_l
        weight = self.weight
        smoothing = self.smoothing
        l = len(X)
        u = len(Z)
        n = l + u

        if self.eta:
            data = pd.concat(X, Z)
            U_uu = np.ones((n, n))/(n + l)
            U_ul = np.ones((n, l))/(n + l)
            W_uu = weight(data, data)
            d = np.sum(W_uu[:l], axis=1) - W_uu[range(l), range(l)]
            W_ul = np.zeros((n, l))
            W_ul[range(l), range(l)] = self.eta * d/(1 - self.eta)
            D_uu_inv = np.diag(1/(np.sum(W_ul, axis=1) + np.sum(W_uu, axis=1)))
            P_uu = smoothing * U_uu + (1 - smoothing) * D_uu_inv @ W_uu
            P_ul = smoothing * U_ul + (1 - smoothing) * D_uu_inv @ W_ul
        else:
            U_uu = np.ones((u, u)) / n
            U_ul = np.ones((u, l)) / n
            W_ul = weight(Z, X)
            W_uu = weight(Z, Z)
            D_uu_inv = np.diag(1/(np.sum(W_ul, axis=1) + np.sum(W_uu, axis=1)))
            P_uu = smoothing * U_uu + (1 - smoothing) * D_uu_inv @ W_uu
            P_ul = smoothing * U_ul + (1 - smoothing) * D_uu_inv @ W_ul

        A = np.eye(u) - P_uu
        B = P_ul
        # f_u = np.linalg.solve(A, B @ f_l)
        f_u = bicgstab(A, B @ f_l)[0]
        # TODO Make result an object
        result = f_u
        if display:
            weight_matrix = np.linalg.solve(A, B)
            influence_matrix = weight_matrix * (2 * f_l - 1)
            raw_mean = weight_matrix * (2 * (f_l - f_u[:, None]))**2
            predictive_uncertainty = np.sum(raw_mean, axis=1)**0.5
            result = f_u, influence_matrix, predictive_uncertainty
        return result

    def squared_error(self, Z, y, theta=None,
                      eval_gradient=False):
        '''Evaluate the Mean Sqared Error and gradient using the trained Gaussian
        field model on a dataset.

        Parameters
        ----------
        Z: 2D array or list of objects
            Feature vectors or other generic representations of unlabeled data.

        y: 1D array or list of objects
            Label values for Data.

        theta: 1D array or list of objects
            Hyperparameters for the weight class

        eval_gradients:
            Whether or not to evaluate the gradients.

        Returns
        -------
        err: 1D array
            Mean Squared Error

        grad: 1D array
            Gradient with respect to the hyperparameters.
        '''

        if len(self.X) == 0:
            raise Exception("Missing Training Data")
        if len(self.f_l) == 0:
            raise Exception("Missing Training Labels")
        if theta is not None:
            self.weight.theta = theta

        predictions = self.predict(Z)

        f_u = predictions[-len(Z):]
        e = f_u - y
        err = 0.5 * np.sum(e**2)/len(f_u)
        if eval_gradient is True:
            grad = np.zeros(len(self.weight.theta))
            for i in range(len(self.weight.theta)):
                eps = self.eps
                self.weight.theta += eps
                f1 = self.squared_error(Z, y, theta)
                self.weight.theta -= 2 * eps
                f2 = self.squared_error(Z, y, theta)
                self.weight.theta += eps
                grad[i] = (f1 - f2)/(2 * eps)
            return err, grad
        else:
            return err

    def cross_entropy(self, Z, y, theta=None, eval_gradient=False):
        '''Evaluate the Cross Entropy gradient using the trained Gaussian field
        model on a dataset.

        Parameters
        ----------
        X: 2D array or list of objects
            Feature vectors or other generic representations of unlabeled data.

        y: 1D array or list of objects
            Label values for Data.

        theta: 1D array or list of objects
            Hyperparameters for the weight class

        eval_gradients:
            Whether or not to evaluate the gradients.

        Returns
        -------
        y: 1D array
            Predicted values of the input data.

        grad: 1D array
            Gradient with respect to the hyperparameters.
        '''
        if len(self.X) == 0:
            raise Exception("Missing Training Data")
        if len(self.f_l) == 0:
            raise Exception("Missing Training Labels")

        if theta is not None:
            self.weight.theta = theta

        f_u = self.predict(Z)
        err = -y @ np.log(f_u) - (1 - y) @ np.log(1 - f_u)
        if eval_gradient is True:
            grad = np.zeros(len(self.weight.theta))
            for i in range(len(self.weight.theta)):
                eps = self.eps
                self.weight.theta += eps
                f1 = self.cross_entropy(Z, y, theta)
                self.weight.theta -= 2 * eps
                f2 = self.cross_entropy(Z, y, theta)
                self.weight.theta += eps
                grad[i] = (f1 - f2)/(2 * eps)
            return err, grad
        else:
            return err

    def average_label_entropy(self, Z, y, theta=None,
                              eval_gradient=False):
        '''Evaluate the Average Label Entropy gradient using the trained Gaussian
        field model on a dataset.

        Parameters
        ----------
        Z: 2D array or list of objects
            Feature vectors or other generic representations of unlabeled data.

        y: 1D array or list of objects
            Label values for Data.

        theta: 1D array or list of objects
            Hyperparameters for the weight class

        eval_gradients:
            Whether or not to evaluate the gradients.

        Returns
        -------
        err: 1D array
            Average Label Entropy

        grad: 1D array
            Gradient with respect to the hyperparameters.
        '''
        if len(self.X) == 0:
            raise Exception("Missing Training Data")
        if len(self.f_l) == 0:
            raise Exception("Missing Training Labels")

        if theta is not None:
            self.weight.theta = theta

        predictions = self.predict(Z)
        f_u = predictions[-len(Z):]
        err = (-f_u @ np.log(f_u) - (1 - f_u) @ np.log(1 - f_u))/len(f_u)
        if eval_gradient is True:
            grad = np.zeros(len(self.weight.theta))
            for i in range(len(self.weight.theta)):
                eps = self.eps
                self.weight.theta += eps
                f1 = self.average_label_entropy(Z, y, theta)
                self.weight.theta -= 2 * eps
                f2 = self.average_label_entropy(Z, y, theta)
                self.weight.theta += eps
                grad[i] = (f1 - f2)/(2 * eps)
            return err, grad
        else:
            return err

    def laplacian_error(self, theta=None, eval_gradient=False):
        '''Evaluate the Laplacian Error and gradient using the trained Gaussian
        field model on a dataset.

        Parameters
        ----------
        theta: 1D array or list of objects
            Hyperparameters for the weight class

        eval_gradients:
            Whether or not to evaluate the gradients.

        Returns
        -------
        err: 1D array
            Laplacian Error

        grad: 1D array
            Gradient with respect to the hyperparameters.
        '''
        if len(self.X) == 0:
            raise Exception("Missing Training Data")
        if len(self.f_l) == 0:
            raise Exception("Missing Training Labels")
        if theta is not None:
            self.weight.theta = theta
        smoothing = self.smoothing
        weight = self.weight
        f_l = self.f_l
        X = self.X
        W = weight(X, X)
        U_ll = np.ones((len(f_l), len(f_l))) * 1/len(f_l)
        D_inv = np.diag(1/np.sum(W, axis=1))
        h = (smoothing * (U_ll @ f_l)) + ((1 - smoothing) * D_inv @ (W @ f_l))
        err = h @ h
        if eval_gradient is True:
            grad = np.zeros(len(self.weight.theta))
            for i in range(len(self.weight.theta)):
                eps = self.eps
                self.weight.theta += eps
                f1 = self.laplacian_error(theta)
                self.weight.theta -= 2 * eps
                f2 = self.laplacian_error(theta)
                self.weight.theta += eps
                grad[i] = (f1 - f2)/(2 * eps)
            return err, grad
        else:
            return err
