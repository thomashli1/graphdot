setuptools
numpy
pandas
networkx
ase
pymatgen
pysmiles
mendeleev
sphinx
sphinx-rtd-theme
treelib
kahypar
numba
m2r
